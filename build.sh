#!/bin/bash

# Build STAPL GitBook
npm install
./node_modules/.bin/gitbook install ./docs
./node_modules/.bin/gitbook build ./docs
mv docs/_book public/docs
