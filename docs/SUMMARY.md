# Summary

* [Introduction](README.md)
* [Getting Started](getting-started.md)
* [Doxygen Reference](https://parasol-lab.gitlab.io/stapl)
* [SGL](sgl/README.md)
  * [Tutorial](sgl/basic-usage/README.md)
  * [Algorithm Reference](sgl/algo-ref/README.md)
    * [Traversals](sgl/algo-ref/traversals/README.md)
      * [Breadth-first search](sgl/algo-ref/raw/breadth_first_search_namespacestapl_1a3f0435999b68917d41c2f26b2dd728c8.md)
  * [Graph Creation](sgl/creation/README.md)
    * [Generators](sgl/creation/generators/README.md)
      * [Barabasi-Albert](sgl/creation/generators/make_barabasi_albert.md)
      * [Binary Tree](sgl/creation/generators/make_binary_tree.md)
      * [Binary Tree Network](sgl/creation/generators/make_binary_tree_network.md)
      * [Complete](sgl/creation/generators/make_complete.md)
      * [Cycle Chain](sgl/creation/generators/make_cycle_chain.md)
      * [Erdos-Renyi](sgl/creation/generators/make_erdos_renyi.md)
      * [Grid](sgl/creation/generators/make_grid.md)
      * [List](sgl/creation/generators/make_list.md)
      * [Lists Tree](sgl/creation/generators/make_lists_tree.md)
      * [Lollipop](sgl/creation/generators/make_lollipop.md)
      * [Mesh](sgl/creation/generators/make_mesh.md)
      * [Newman-Watts-Strogatz](sgl/creation/generators/make_newman_watts_strogatz.md)
      * [Random DAG](sgl/creation/generators/make_random_dag.md)
      * [Random Neighborhood](sgl/creation/generators/make_random_neighborhood.md)
      * [Sparse Mesh](sgl/creation/generators/make_sparse_mesh.md)
      * [Star](sgl/creation/generators/make_star.md)
      * [Torus 3D](sgl/creation/generators/make_torus_3D.md)
      * [Torus 2D](sgl/creation/generators/make_torus.md)
      * [Unconnected Cycles](sgl/creation/generators/make_unconnected_cycles.md)
      * [Watts-Strogatz](sgl/creation/generators/make_watts_strogatz.md)
    * [I/O](sgl/creation/io/README.md)
      * [Edge List](sgl/creation/io/edge-list.md)
      * [Adjacency List](sgl/creation/io/adj-list.md)
      * [Matrix Market](sgl/creation/io/mtx.md)
      * [DIMACS](sgl/creation/io/dimacs.md)
      * [Sharded](sgl/creation/io/sharded.md)
      * [Dot](sgl/creation/io/dot.md)
    * [Manual Creation](sgl/creation/manual.md)
    * [Partitioning](sgl/creation/partition/README.md)
      * [Implicit](sgl/creation/partition/implicit.md)
      * [Repartitioning](sgl/creation/partition/gpartition.md)
      * [View-Based](sgl/creation/partition/view-based.md)
  * [Writing Algorithms](sgl/algorithms/README.md)
    * [SGL Model](sgl/algorithms/model.md)
    * [KLA Paradigm](sgl/algorithms/kla.md)
    * [Execute Options](sgl/algorithms/options.md)
  * [FAQ](sgl/faq/README.md)
