# pscc

Defined in <`stapl/containers/graph/algorithms/pscc.hpp`>

```c++
template<typename GraphView>  
void pscc(GraphView g,double pivot_count)
```

## Summary

A Parallel Strongly-Connected Components Algorithm (pSCC)

This function executes the DCSCMulti algorithm on the input graph. It begins by selecting approximately pivot_count pivots and then adaptively selects a new value on each iteration. The SCC color is stored in the property using a set_cc() function on the graph's vertex property. 
#### Parameters
* *g*: The graph_view over the input graph. 


* *pivot_count*: The number of pivots to select at each iteration.
