# nested_sharded_graph_reader

Defined in <`stapl/containers/graph/algorithms/sharded_graph_io.hpp`>

```c++
template<typename Graph,typename LineReader>  
graph_view< Graph > nested_sharded_graph_reader(std::string filename,LineReader line_reader)
```

## Summary

Algorithm to read a graph that is sharded in a nested way.

Uses the user-provided reader to read each line and add the edges from the line to the graph.

Input graph must be sharded using the nested sharder. A sharded file is split into multiple input files and a metadata file that keeps track of the number of vertices, edges, total shards and nested dimensions.

An example metadata file would be: 1000 1000000 128 2 100

Each line after the first line represents the shape of the space. The files would then be in nested directories (e.g., base/1/24)


#### Parameters
* *filename*: The name of the input metadata file. 


* *line_reader*: The functor used to read-in a line of text from file and add the appropriate edges to the provided aggregator, LineReader is given a stream with the line and an Edge-Aggregator. It should read the stream and add the appropriate edges to the provided Aggregator. LineReader should return '0' for success, '1' for failure.





#### Returns
A graph_view over the output graph, populated with the vertices and edges from the input file.
