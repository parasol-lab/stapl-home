# graph_sharder

Defined in <`stapl/containers/graph/algorithms/graph_io.hpp`>

```c++
inline void graph_sharder(std::string filename,size_t shard_size,bool removecomments)
```

## Summary

Function to shard an input file for efficient parallel reading.

Files sharded by the graph sharder can be read by [sharded_graph_reader()](#namespacestapl_1ad0172cb7c875afce6b857fc2fb12fb00). This function is not parallel and works from one location. 
#### Parameters
* *filename*: The name of the input file. The input file must contain the number of vertices and edges in the graph in the first line. The output shards created will be named with the provided filename as the prefix, followed by the shard-ID. A metadata file with the name filename.metadata will be created containing the metadata for the shards (#vertices, #edges, #shards, etc.). 


* *shard_size*: The size (in number of lines) of each output shard. 


* *removecomments*: Set to true for excluding commented lines from shards, or false to keep the commented lines in the output shards.





> Todo: Find a suitable default for shard_size parameter.
