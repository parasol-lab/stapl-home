# read_edge_list

Defined in <`stapl/containers/graph/algorithms/graph_io.hpp`>

```c++
template<typename Graph>  
graph_view< Graph > read_edge_list(std::string filename,size_t blk_sz)
```

## Summary

Function to wrap around file-reader to support reading an edge-list.


