# nc_map_func

Defined in <`stapl/containers/graph/algorithms/nc_map.hpp`>

```c++
template<typename Functor,typename View0,typename... View>  
void nc_map_func(Functor const & func,View0 const & view0,View const &... view)
```

## Summary

map_func() variant that does not coarsen the input views if size of `view0` is less than the number of locations.

This is needed to make map-func work with views with sizes less than the number of locations until the metadata reported is corrected to exclude empty base containers.
