# h_paradigm

Defined in <`stapl/containers/graph/algorithms/paradigms/h_paradigm.hpp`>

```c++
template<typename WF,typename UF,typename PostExecute,typename HView,typename GView>  
size_t h_paradigm(WF const & uwf,UF const &,PostExecute post_execute,HView & h,GView & g)
```

## Summary

The Parallel Hierarchical Level-Synchronous (h) Paradigm.

Implements the hierarchical level-sync paradigm, which iteratively executes BSP-Supersteps (SS). Each SS applies the user provided vertex-operator over the vertices of the input graph. Any communication generated within a SS is guaranteed to have finished before the SS finishes. The communication happens hierarchically, by following the locality of the graph through the machine-hierarchy. Updates (neighbor-operators) to be applied to remote neighbors of a vertex v are sent to v's parent vertex in the machine hierarchy. The parent then forwards the update to all its neighbors that contain neighbors of v. When the update is received by the parent's neighbor, it applies the update to all of its children that are neighbors of v in the lower-level graph. This allows us to send a single update per remote location vs. sending an update per remote/cross edge. Next, the local neighbors of v are updated. This may lead to a reduction in the amount of communication from O(E) to O(V), providing faster and more scalable performance.

The user provides a vertex-operator to express the computation to be performed on each vertex, and a neighbor-operator that will be applied to each neighbor that is visited. The vertex-operator is passed in a vertex and a visit object. To visit a neighboring vertex, the vertex-operator must call visit_all_edges(neighbor, neighbor_operator()). The vertex-operator must return true if the vertex was active (i.e. its value was updated), or false otherwise. The neighbor-operator should be instantiated within vertex-operators and passed to a visit method to be applied to a vertex's neighbors. It is passed in a neighboring vertex. Neighbor-operators instantiated within vertex-operators may carry state, but must be immutable. They should return true if the visit was successful (i.e. the target vertex will be activated after this visit), or false otherwise. Users may also provide additional functions to be executed before and after each SS. 
#### Parameters
* *WF*: The type of the user provided vertex-operator expressing computation to be performed over each vertex. 


* *UF*: The type of the user provided neighbor-operator expressing computation to be performed over neighboring vertices. 





#### Parameters
* *post_execute*: Optional functor that will be executed on the graph_view at the end of each SS. This will be invoked with the input graph_view and the current SS ID (the ID of the SS that just finished). 


* *h*: The hierarchical machine graph_view over the input graph. This must be created using [create_level_machine()](#namespacestapl_1a642f8180b57c54c687fdccf799ec05bb). 


* *g*: The graph_view over the input graph. 





#### Returns
The number of iterations performed by the paradigm.
