# k_core_dynamic

Defined in <`stapl/containers/graph/algorithms/k_core_dynamic.hpp`>

```c++
template<typename GView>  
size_t k_core_dynamic(GView & graph,size_t k)
```

## Summary

A Parallel k-core algorithm. Iteratively deletes all vertices with out-degree smaller than k.

#### Parameters
* *graph*: The graph_view over the input graph. 


* *k*: The parameter to control which vertices are deleted.
