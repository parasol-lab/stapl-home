# link_prediction

Defined in <`stapl/containers/graph/algorithms/link_prediction.hpp`>

```c++
template<class GView>  
void link_prediction(GView & g,size_t k)
```

## Summary

Predicts the probability that an edge (that does not exist yet) will be added.

The algorithm is based on the Adamic-Adar model [^1], where the probability of an edge (x,y) is given by: $$ AA(x,y) = Sum_{n \in Common-neighbors(x,y)}(\frac{1}{\log(degree(n))}) $$

A value of 1 indicates the edge exists already, and a value of zero is the lowest.


#### Parameters
* *g*: The graph_view over the input graph.


* *k*: The maximum amount of asynchrony allowed in each phase. 0 <= k <= inf. k == 0 implies level-synchronous paradigm. k >= D implies fully asynchronous paradigm (D is diameter of graph).





**See also**: [properties::lp_property](#classstapl_1_1properties_1_1lp__property)

#### Example
```cpp
  using property_type = stapl::properties::lp_property;
  using graph_type = stapl::multidigraph<property_type>;

  auto g = stapl::read_edge_list<graph_type>(argv[1]);

  stapl::link_prediction(g, 0);

  auto probs = g[0].property().link_probabilities();

  std::cout << "Predicted links for vertex 0:" << std::endl;
  std::copy(probs.begin(), probs.end(),
      std::ostream_iterator<double>(std::cout, " "));
```
 [^1] Adamic, Lada A., and Eytan Adar. "Friends and neighbors on the web." Social networks 25.3 (2003): 211-230.
