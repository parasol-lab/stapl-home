# cc_stats

Defined in <`stapl/containers/graph/algorithms/connected_components.hpp`>

```c++
template<typename GView>  
std::vector< std::pair< typename GView::vertex_descriptor, size_t > > cc_stats(GView & g)
```

## Summary

Algorithm to compute the number of vertices belonging to each connected-component in the graph, given the connected-component of each vertices.

#### Parameters
* *g*: The graph_view over the input graph. property_type on the vertex needs to have methods: vertex_descriptor cc(void)  for getting the connected-component id. cc(vertex_descriptor)  for setting the connected-component id. The connected-component for each vertex must be available, or be filled-in by calling connected_components(g) algorithm before calling cc_stats. 





#### Returns
A vector of pairs containing the CCID and the number of vertices in that connected-component, for each CC. [Size: O(num CC)] 


> Todo: The std::vector being returned here should be a pContainer populated using the butterfly skeleton.
