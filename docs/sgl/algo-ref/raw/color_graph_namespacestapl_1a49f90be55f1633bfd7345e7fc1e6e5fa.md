# color_graph

Defined in <`stapl/containers/graph/algorithms/graph_coloring.hpp`>

```c++
template<typename GView,typename ColorMap>  
void color_graph(GView const & graph,ColorMap & color_map)
```

## Summary

Parallel Level-Synchronized Graph Coloring Algorithm.

Assigns coloring to the vertices of the input graph, such that neighboring vertices are not assigned the same color. 
#### Parameters
* *graph*: The graph_view over the input graph. 


* *color_map*: The output vertex property map where the color information will be stored. [vertex->color (int)]
