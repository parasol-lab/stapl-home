# graph_metrics

Defined in <`stapl/containers/graph/algorithms/graph_metrics.hpp`>

```c++
template<typename Graph,typename VertexPartitionMap>  
[metrics_info](#structstapl_1_1metrics__info) graph_metrics(Graph & g,VertexPartitionMap & vertex_partition_map)
```

## Summary

Algorithm to compute various metrics for the provided input graph, based on the graph-partition provided.

These metrics are useful for scientific-computing codes that work on partitioned meshes. The metrics produce information about the quality of the partition. These metrics are based on those requested by a National Lab.

#### Parameters
* *g*: The graph_view over the input graph. 


* *vertex_partition_map*: The input vertex property map specifying the partition that each vertex belongs to. 





#### Returns
A [metrics_info](#structstapl_1_1metrics__info) object containing information about the quality of the specified partition for the input graph.
