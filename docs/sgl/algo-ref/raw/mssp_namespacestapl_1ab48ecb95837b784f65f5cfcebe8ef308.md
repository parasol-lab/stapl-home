# mssp

Defined in <`stapl/containers/graph/algorithms/mssp.hpp`>

```c++
template<class GView>  
size_t mssp(GView & g,std::vector< typename GView::vertex_descriptor > const & sources,size_t k)
```

## Summary

A Parallel Multi-Source Shortest Paths (MSSP) algorithm.

Performs a multi-source shortest path query on the input graph_view, storing the shortest path distances and parents on each reachable vertex for each traversal. All vertices will be initialized with their distances as infinity (MAX) and their active states as empty, except the source-vertex for each traversal, which will have its distance set to zero (0) and active state set to true for that traversal. Parents of each vertex will be initialized to the vertex's descriptor. 
#### Parameters
* *g*: The graph_view over the input graph. 


* *sources*: The descriptors of the source vertices for this traversal. 


* *k*: The maximum amount of asynchrony allowed in each phase. 0 <= k <= inf. k == 0 implies level-synchronous MSSP. k >= D implies fully asynchronous MSSP (D is diameter of graph). 





#### Returns
The number of iterations performed by the paradigm.


**See also**: [properties::mssp_property](#classstapl_1_1properties_1_1mssp__property)

#### Example
```cpp
  using property_type = stapl::properties::mssp_property;
  using graph_type = stapl::multidigraph<property_type, int>;

  auto g = stapl::read_weighted_edge_list<graph_type>(argv[1]);

  std::vector<std::size_t> sources{{20,21,22}};

  std::size_t iters = stapl::mssp(g, sources, 10);

  int dist = g[0].property().distance(sources.front());

  std::cout << "vertex 0 is distance " << dist
              << " away from vertex " << sources.front();
```
