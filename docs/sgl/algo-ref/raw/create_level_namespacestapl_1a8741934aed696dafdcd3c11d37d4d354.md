# create_level

Defined in <`stapl/containers/graph/algorithms/hierarchical_view.hpp`>

```c++
template<typename GraphVw,typename VertexPartitioner,typename EdgeFunctor>  
[hierarchical_graph_view](#classstapl_1_1hierarchical__graph__view)< dynamic_graph< DIRECTED, MULTIEDGES, [hierarchical_graph_view](#classstapl_1_1hierarchical__graph__view)< typename GraphVw::view_container_type, typename [extract_domain_type](#structstapl_1_1extract__domain__type)< GraphVw, VertexPartitioner >::type >, typename EdgeFunctor::value_type > > create_level(const GraphVw & graph,const VertexPartitioner & partitioner,const EdgeFunctor & ef,size_t level)
```

## Summary

Algorithm to create a level in the hierarchy of graphs, using the user-provided vertex partitioner and edge-functor.

#### Parameters
* *graph*: A graph_view over the input graph. 


* *partitioner*: The vertex paritioner which takes an input graph and returns the partition of vertices on next level, along with descriptors corresponding to them. 


* *ef*: The edge functor which takes graph of the next level, and adds the required edges. 


* *level*: The level of this view in the hierarchy. 





#### Returns
A [hierarchical_graph_view](#classstapl_1_1hierarchical__graph__view) over the graph of the next-level.
