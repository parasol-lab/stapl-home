# graph_reader

Defined in <`stapl/containers/graph/algorithms/graph_io.hpp`>

```c++
template<typename Graph,typename LineReader,typename HeaderReader>  
graph_view< Graph > graph_reader(std::string filename,LineReader line_reader,HeaderReader header_reader,size_t blk_sz)
```

## Summary

Algorithm to read a graph from the given input file.

Uses the user-provided reader to read each line and add the edges from the line to the graph.

#### Parameters
* *filename*: The name of the input file. 


* *line_reader*: The functor used to read-in a line of text from file and add the appropriate edges to the provided aggregator, LineReader is given a stream with the line and an Edge-Aggregator. It should read the stream and add the appropriate edges to the provided Aggregator. LineReader should return '0' for success, '1' for failure. 


* *blk_sz*: The size of the block (in number of lines), in which the input file will be partitioned to be read. 





#### Returns
A graph_view over the output graph, populated with the vertices and edges from the input file. 


#### Parameters
* *header_reader*: A custom header reader to parse the header of a file and extract number of vertices.
