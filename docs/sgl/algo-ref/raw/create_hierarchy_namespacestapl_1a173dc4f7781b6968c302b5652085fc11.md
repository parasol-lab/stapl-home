# create_hierarchy

Defined in <`stapl/containers/graph/algorithms/create_hierarchy.hpp`>

```c++
template<typename GraphView,typename Coarsener,typename VertexPReducer,typename EdgePReducer,typename Done>  
std::vector< GraphView > create_hierarchy(GraphView & gvw,Coarsener const & coarsener,VertexPReducer const & vpr,EdgePReducer const & epr,Done const & done,size_t max_msg_sz,bool self_edges,bool sort_edges)
```

## Summary

Algorithm to create multiple levels of hierarchy at once.

Creates multiple levels of hierarchy based on the input graph_view and the provided vertex-grouping algorithm. Supervertices and superedges in each level store descriptors of their children in the level below. Properties of supervertices and superedges are reductions of the properties of their children through the user provided reducers.

#### Parameters
* *gvw*: The input graph_view over which the new levels are to be created. The underlying graph container must be DIRECTED, NONMULTIEDGES, and store the custom [super_vertex_property](#structstapl_1_1super__vertex__property) and [super_edge_property](#structstapl_1_1super__edge__property) on their vertices and edges, respectively. 


* *coarsener*: A functor which takes an input graph_view and level-id and outputs an std::pair of bool indicating if the matching information is complete (true) or partial (false), and a vertex property map identifying the "group" of each vertex in the graph. The leader-vertex of each group is the vertex whose group ID is the same as its descriptor. There must be exactly one leader vertex in each group. Non-leader vertices may choose to "collapse" with other leader or non-leader vertices. The final grouping with which to create the supervertex is calculated by running a pointer-jumping (or similar) algorithm. [vertex->vertex_descriptor] 


* *vpr*: The vertex property reducer for reducing child-vertex properties to form the supervertex property. 


* *epr*: The edge property reducer for reducing child-edge properties to form the superedge property. 


* *done*: A functor which takes an input graph_view and level-id and outputs if another level of hierarchy should be created. [bool] 


* *max_msg_sz*: The number of requests to aggregate for the aggregator. 


* *self_edges*: Indicates whether self-edges are allowed on the output graph or not. If the supergraph has self-edges, they will represent the internal edges between the supervertex's child vertices. 


* *sort_edges*: Indicates if the adjacent-edges for each supervertex in the output supergraph will be sorted based on their target-descriptors. 





#### Returns
An std::vector of graph_view representing the final hierarchy. The type of the view is the same as the input graph_view. Size of the output reflects the number of levels in the hierarchy, and hierarchy[i] represents the graph at the i-th level in the hierarchy, with hierarchy[0] being the original input graph.
