# rebalance_diffusive

Defined in <`stapl/containers/graph/algorithms/rebalance_diffusive.hpp`>

```c++
template<typename C,typename W,typename M,typename IS>  
void rebalance_diffusive(C & v,W const & weight_map,M const & migration_action_map,IS const & independent_sets)
```

## Summary

Rebalance a graph diffusively by iteratively moving vertices between neighboring partitions.

Balances the graph based on vertex-weights by migrating vertices to create an even weight-distribution among the locations, while attempting to minimize total movement of vertices.

#### Parameters
* *v*: The graph_view over the input graph. 


* *weight_map*: The vertex property map storing weights for each vertex. 


* *migration_action_map*: A map from vertex descriptor to functor describing an action to perform when a vertex is migrated. This map must export an interface compatible with std::map, where the function to execute has the signature (value_type, gid_type, location_type) 


* *independent_sets*: A collection of independent sets, where each independent set is represented as a vector of edges (pairs of vertices). An example independent set that can be used is linear_independent_sets()




Invalidates the input graph_view.
