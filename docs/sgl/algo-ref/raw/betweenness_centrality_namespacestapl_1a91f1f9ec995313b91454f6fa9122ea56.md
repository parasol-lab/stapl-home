# betweenness_centrality

Defined in <`stapl/containers/graph/algorithms/betweenness_centrality.hpp`>

```c++
template<typename GView,typename HView>  
void betweenness_centrality(GView & gv,HView & hv,size_t num_sources)
```

## Summary

Computes betweenness centrality value of unweighted, directed graph using the hierarchical machine paradigm (H_paradigm()).

#### Parameters
* *gv*: View of graph on which centrality will be computed. 


* *hv*: The hierarchical machine graph_view over the input graph. This is generated by calling [create_level_machine()](#namespacestapl_1a642f8180b57c54c687fdccf799ec05bb) on the input graph_view. 


* *num_sources*: Number of sources to process at a time, if num_sources equals zero then all sources are processed at the the same time. 





> Todo: Explore activation of non-contiguous ranges of vertices.
