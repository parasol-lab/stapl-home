# connected_components

Defined in <`stapl/containers/graph/algorithms/connected_components.hpp`>

```c++
template<class GView>  
size_t connected_components(GView & g,size_t k)
```

## Summary

A Parallel Connected Components (CC) Algorithm.

Marks all vertices belonging to the same CC with the same connected-component ID. 
#### Parameters
* *g*: The graph_view over the input graph. property_type on the vertex needs to have methods: vertex_descriptor cc(void)  for getting the connected-component id. cc(vertex_descriptor)  for setting the connected-component id. bool active(void)  for checking if vertices are active. void active(bool)  for marking vertices as active. 


* *k*: The maximum amount of asynchrony allowed in each phase. 





#### Returns
The number of iterations performed by the algorithm.


**See also**: [properties::cc_property](#classstapl_1_1properties_1_1cc__property)

#### Example
```cpp
  using property_type = stapl::properties::cc_property;
  using graph_type = stapl::multidigraph<property_type>;

  auto g = stapl::read_edge_list<graph_type>(argv[1]);

  stapl::connected_components(g, 0);

  bool connected = stapl::is_same_cc(g, 0, 1);

  std::cout << "vertices 0 and 1 are "
            << (connected ? "" : "not ")
            <<  "in the same cc";
```
