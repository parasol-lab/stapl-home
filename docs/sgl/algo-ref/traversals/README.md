# Traversals

Traversals are fundamental operations in many graph processing workloads.

* [Breadth-first search](sgl/algo-ref/raw/breadth_first_search_namespacestapl_1a3f0435999b68917d41c2f26b2dd728c8.md)
