# Hybrid Out-of-Core Paradigm

The hybrid out-of-core paradigm[^1] is useful for processing graphs that do not fit in the available memory of the system. The paradigm uses sub-graph paging and defered asynchronous execution to efficiently utilize the available RAM to process the graph, while adding no overhead if the system has sufficient available RAM to process the graph.

To use the paradigm, we can simply replace the graph_paradigm with the storage_graph_paradigm, and specify a block-size (in Bytes) indicating the available RAM per core:

```c++
storage_graph_paradigm(BFSVertexOp(), BFSNeighborOp(), graph, blk_sz_bytes);
```

---

[^1] *Harshvardhan, Brandon West, Adam Fidel, Nancy M. Amato, Lawrence Rauchwerger, "A Hybrid Approach To Processing Big Data Graphs on Memory-Restricted Systems," In Proc. Int. Par. and Dist. Proc. Symp. (IPDPS), Hyderabad, India, May 2015.*
