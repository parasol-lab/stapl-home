# DOT

For visualization purposes, we provide a way to dump a graph view to a [DOT](https://en.wikipedia.org/wiki/DOT_(graph_description_language)
file to use in an external viewer.

## Writing

To write a graph to a file in the DOT format, use:

```c++
write_dot(G g, std::string filename);
```
