# make_lists_tree

Defined in <`stapl/containers/graph/generators/lists_tree.hpp`>

```c++
template<typename GraphView>  
GraphView make_lists_tree(size_t nx,size_t ny,bool bidirectional)
```

## Summary

Generate a tree-of-lists graph.

The generated graph is a tree of x branches, each of which is a list containing y vertices.

The returned view owns its underlying container.


#### Parameters
* *nx*: Number of branches in the tree. 


* *ny*: Number of vertices in each list. 


* *bidirectional*: True for adding back-edges, False for forward only. 





#### Returns
A view over the generated graph.

#### Example
```cpp
  using view_type = stapl::graph_view<stapl::multidigraph<int>>;

  auto v = stapl::generators::make_lists_tree<view_type>(16, 4);
```
